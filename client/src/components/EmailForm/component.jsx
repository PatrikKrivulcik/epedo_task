import { useState } from "react";
import "./style.css";

const Form = () => {
	const apiUrl = "http://localhost:3000";
	const [email, setEmail] = useState("");

	const formSend = () => {
		if (email == null || !email) {
			alert("Email nemôže byť prázdny.");
			return false;
		}

		fetch(`${apiUrl}/api/contact/check`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
			}),
		})
			.then((res) => {
				if (res.status === 200) {
					return res.json();
				}
				alert("Vyskytla sa chyba aplikácie, kontaktujte nás prosím.");
			})
			.then((res) => {
				if (res) {
					if (res.available) {
						emailInsert();
						return false;
					}
					alert("Email je už použitý alebo ma zlý formát");
				}
			})
			.catch((e) => {
				alert("Vyskytla sa chyba aplikácie, kontaktujte nás prosím.");
				console.log(e);
			});
	};

	const emailInsert = () => {
		fetch(`${apiUrl}/api/contact`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
			}),
		})
			.then((res) => {
				if (res.status === 200) {
					alert("Boli ste úspešne zaregistrovaný");
					return false;
				}
				alert("Vyskytla sa chyba aplikácie, kontaktujte nás prosím.");
			})
			.catch((e) => {
				alert("Vyskytla sa chyba aplikácie, kontaktujte nás prosím.");
				console.log(e);
			});
	};

	return (
		<div className="emailForm">
			<form>
				<input
					type="email"
					placeholder="E-mail"
					required
					value={email}
					onChange={(e) => {
						setEmail(e.target.value);
					}}
				/>
				<input
					type="submit"
					value="Odoslať"
					onClick={(e) => {
						e.preventDefault();
						formSend();
					}}
				/>
			</form>
		</div>
	);
};

export default Form;
