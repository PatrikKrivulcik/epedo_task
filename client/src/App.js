import Form from "./components/EmailForm/component";

function App() {
  
	return (
		<div className="App">
			<Form />
		</div>
	);
}

export default App;
