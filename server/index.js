const PORT = 3000;
const emails = [];

const app = require("express")();
app.use(require("cors")());
app.use(require("body-parser")());

app.get("/api/contacts", (req, res) => {
	res.json(emails);
});

app.post("/api/contact/check", (req, res) => {
	if (
		!req.body.email ||
		!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
			req.body.email
		) ||
		emails.indexOf(req.body.email) != -1
	) {
		res.json({
			available: false,
		});
	} else {
		res.json({
			available: true,
		});
	}
});

app.put("/api/contact", (req, res) => {
	if (!req.body.email) {
		res.sendStatus(400);
		return false;
	}

	emails.push(req.body.email);
	res.sendStatus(200);
});

app.listen(PORT, () => {
	console.log(`[Server listening on ${PORT}]`);
});
